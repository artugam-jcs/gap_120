---
title: README.md
date: 2023-03-21 14:00:16
description: 
---

#### 1. List Karl the Robot’s 5 primitive instructions

1. `move`
2. `turnleft`
3. `turnoff`
4. `pickbeeper`
5. `putbeeper`

#### 2. List 5 additional primitives from the Lua language.

1. `string`
2. `char`
3. `int`
4. `float`
5. `boolean`

#### 3. If a computer is given 1 byte to remember what level the user is on, what is the maximum number of levels that the game will be able to have?

```
2^8 = 256
```

#### 4. You are working on a rock-paper-scissors game where the first player to score 5 points wins. What data type would you use to remember the score? What is an example of a value that it might contain? How will this value be represented in bits?

The minimum bits is `3`

```
2^3 = 8
```

Since there is no data type for 3 bits, we use `byte` instead. Therefore, 2^8 bits.

From `0000 0000` to `0000 0101` (`0` to `5`).

#### 5. You are working on a hangman game where the player types in one letter at a time to guess. What data type would you use to remember the user’s guess? What is an example of a value that it might contain? How will this value be represented in bits?

`char` is the best case data type to use.

The example, is `'c'`.

A `char` is a `byte`, therefore, the date will be represented in 8 bits.

From `0000 0000` to `1111 1111` (0 to 256).

In our case `'c'` will be `0110 0011`.

#### 6. How many places can contain beepers? What are their coordinates?

20

#### 7. How many total beepers can there be?

The max is `20 * 2` = `40`

#### 8. An intern was given the following specification: Collect 3 beepers. She then wrote this program:

```lua
function main()
    move();
    pickbeeper();
    move();
    pickbeeper();
    turnleft();
    move();
    pickbeeper();
    turnoff();
end
```

#### Does this program meet the specification? If not, explain how it could fail.

Yes, it does.

#### 9. An intern was given the following specification: Collect 6 beepers. He then wrote this program:


```lua
function main()
    function1();
    function1();
    turnleft();
    function1();
    function1();
    turnleft();
    function1();
    function1();
    function1();
    function1();
    function1();  -- The last step will move out of the beeper, is there a wall?
    turnoff();
end

function function1()
    move();
    if(next_to_a_beeper())
    then
        pickbeeper();
    end
end
```

The program does run through, but it will end up outside of the beeper route.

#### 10. Regardless of whether it succeeds, how can the program in question 9 be improved?

1. Rename `function1` to something more readable.
2. Use loop instead of many `function1` calls.
3. More comment
4. Missing document string

#### 11. An intern was given the following specification: Collect 4 beepers. She then wrote this program:

```lua
function TryPickBeeper()
    if (next_to_a_beeper())
    then
        pickbeeper();
    end
end

function Advance()                 -- This function never get calls?
    if (facing_north())
    then
        if (left_is_clear())
        then
            if (front_is_clear())
            then
                move();
                tryPickBeeper();
                turnleft();
                turnleft();
                move();
                turnleft();
                turnleft();
                turnleft();
            end
        end
    end
end

function main()
    tryPickNextBeeper();                -- wrong function name, should be capital
    tryPickNextBeeper();
    tryPickNextBeeper();
    tryPickNextBeeper();
    tryPickNextBeeper();
    tryPickNextBeeper();
    turnoff();
end
```

#### Does this program meet the specification? If not, explain how it could fail (e.g., whatrules it violates).

Function name void `tryPickNextBeeper`, it should be `TryPickNextBeeper`.

#### 12. Regardless of whether it succeeds, how can the program in question 11 be improved?

1. By calling `TryPickNextBeeper` without moving will just stay in one place.
2. Function `Advance` is defined but never been used.

#### 13. An intern was given the following specification: Collect 3 beepers. He then wrote this program:

```lua
function main()
    while front_is_blocked()
    do
        move();
        if (next_to_a_beeper())
        then
            pickbeeper();
        end
    end
    turnoff();
end
```

#### What is the body of the loop? How many iterations will execute? What is the loop’s end condition?

It iterates `1` since nothing is block at the start of the program.

Loop's end condition is when **nothing** is block.

#### 14. Does the program in question 13 meet the specification? Describe what will happen when it executes.

Nothing will happens. It stays at the starting point and shutdown.

#### 15.  An intern wrote this program. You don’t know what the specification was. (Assume it’s unrelated to the specification shown on the previous page.)

```lua
function main()
    turnleft();
    move();
    pickbeeper();
    move();
    pickbeeper();
    turnleft();
    move();
    pickbeeper();
    move();
    pickbeeper();
    turnoff();
end
```

#### Another team member tells you there’s a problem with this program. What are some questions you might ask this team member?

1. Can the program be written with loop(s)?
2. Can the program be more simplified?
3. Can all beepers be picked in one place, the `?`?

#### 16. You have been asked to write a program to meet this specification: Collect all beepers. 

#### Write a Karel program that completes the specification. Use all of the tools at your disposal in the best way possible to receive maximum credit.

```lua
--- Turn the robot 90 degrees to the right
function turnright()
    for _ = 1, 3
    do
        turnleft()
    end
end

--- Make sure we pick up all beepers at place.
function PickAllBeepers()
    while (next_to_a_beeper())
    do
        pickbeeper();
    end
end

--- Move and pick up all beepers.
function MoveAndPick()
    move()
    PickAllBeepers()
end

--- Make movement according to condition.
-- 1. At the corners
-- 2. At the intersection (take care of left only)
function SmartMove()
    local leftBlocked = left_is_blocked()
    local rightBlocked = right_is_blocked()
    local frontBlocked = front_is_blocked()

    if (frontBlocked and rightBlocked)
    -- On left corner
    then
        turnleft()
        MoveAndPick()
    elseif (frontBlocked and leftBlocked)
    -- On right corner
    then
        turnright()
        MoveAndPick()
    elseif (!frontBlocked and !leftBlocked)
    -- On intersection |-
    -- turn left has higer precedent, so no need to worray on right.
    then
        turnleft()
        MoveAndPick()
        turnleft()
        turnleft()
        move()
        turnleft()  -- back to original position
    else
        MoveAndPick()
    end
end

--- Program Entry
function main()
    MoveAndPick()              -- make first move
    while (beepers.no_zero())  -- when beepers is no zero on map
    do
        SmartMove()
    end
end
```

#### 17. Explain what role functions play in your answer to question 16.

- `turnright` - handy version of `turnleft`
- `PickAllBeepers` - make sure all beepers are collected in a place
- `SmartMove` - the most important function to collect all beepers on the map.
- `main` - program entry

#### 18. Explain what role conditionals play in your answer to question 16.

Weather `Karel` is at the corner or insersection; then make the right move.

#### 19. Explain what role loops play in your answer to question 16.

1. Pick up all beepers at a place (in function `PickAllBeepers`)
2. Pick up all beepers in the map (Game Loop in function `main`)
