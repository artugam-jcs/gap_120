@echo off
:: ========================================================================
:: $File: run.bat $
:: $Date: 2023-05-23 14:38:02 $
:: $Revision: $
:: $Creator:  $
:: $Notice: See LICENSE.txt for modification and distribution information
::                   Copyright © 2023 by Shen, Jen-Chieh $
:: ========================================================================

set PATH=%PATH%;D:\Users\jshen3\Downloads\Program Files\Lua

lua54 main.lua
