-- ========================================================================
-- $File: main.lua $
-- $Date: 2023-05-23 14:37:27 $
-- $Revision: $
-- $Creator:  $
-- $Notice: See LICENSE.txt for modification and distribution information
--                   Copyright © 2023 by Shen, Jen-Chieh $
-- ========================================================================

--- Get a choice from the user.
-- @return A string either `l` represent left or `r` represent right.
function GetChoice()
    local choice = "";
    while true do
        choice = io.read()
        if choice == "l" or choice == "r" then
            break
        end
        io.write("[TIP] You must enter either [l]eft or [r]ight, please try again: ")
    end
    return choice;
end

--- Program Entry
function main()
    io.write("You are at a fork in the road\n")
    io.write("Press [l]eft or [r]ight to proceed: ")
    local choice = GetChoice()
    if choice == "l" then
        io.write("Dead end! You lose!\n")
        return;
    end
    io.write("You walk past a lake!\n")
    io.write("Beyond the lake, there's another fork in the road\n")
    io.write("Press [l]eft or [r]ight to proceed: ")
    choice = GetChoice()
    if choice == "r" then
        io.write("Dead end! You lose!\n")
        return;
    end
    io.write("You arrived the castle! You win!\n")
end

main()
